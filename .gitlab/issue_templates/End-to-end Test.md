<!--
The issue description should include any notes describing why this end-to-end test is being added.
Be sure to include any prerequisites or expectations.
-->

## Steps

1. Log into GitLab
1. 

<!--
Labels:

/label ~ui-test
/label ~api-test

/label ~"group::source code"
-->

<!--
When the test is complete and this issue is turned into a report for test results for this test,
you'll need to update the description so that the test-case reporter can find it.

You'll need to add the path beginning from `browser_ui` or `api` as `<filepath stub>`, and the
full description (`<full RSpec description>`) of the test including every enclosing context. The
title is truncated to 255 characters (including ellipses to indicate truncation).

Please use the following format:

Title: <filepath stub> | <full RSpec description>

Description:
### Full description

<full RSpec description>

<Steps and other details from the original issue>

### File path

<filepath>
-->