require 'rest-client'
require 'json'
require 'builder'

## Run it as PRIVATE_TOKEN=<your token> ruby generate_testcases_metrics.rb
## Output: A file named "output.html" will be created in the current directory
def hasharray_to_html( hashArray )
  headers = hashArray.inject([]){|a,x| a |= x.keys ; a}

  html = Builder::XmlMarkup.new(:indent => 2)
  html.table {
    html.tr { headers.each{|h| html.th(h)} }
    hashArray.each do |row|
      html.tr { row.values.each { |value| html.td(value) }}
    end
  }
  return html
end

def get_issue_ids(project_path)
  puts "Fetching issue ids... "
  issue_ids = []
  current_page = 0
  total_pages = 1
  while(current_page <= total_pages.to_i) do
    current_page += 1
    issue_filters = "state=opened&labels=status%3A%3Aautomated&per_page=100&page=#{current_page}"
    response = RestClient.get "#{project_path}/issues?#{issue_filters}"
    total_pages =  response.headers[:x_total_pages]
    JSON.parse(response).each{ |element|
      issue_ids << element["iid"].to_i
    }
  end
  issue_ids
end

def get_label_events(project_path, iid_arr)
  label_events_arr = []
  issue_count = 0

  iid_arr.each { |iid|
    label_events = {}
    current_page = 0
    total_pages = 1
    issue_count += 1
    label_events["Issue ID"] = iid
    # label_events["Issue Title"] = "something"
    puts "Fetching labels for issue id #{iid} ... (#{issue_count} / #{iid_arr.length()})"
    while(current_page <= total_pages.to_i) do
      current_page += 1
      filters = "per_page=100&page=#{current_page}"
      # puts "issue: #{iid} label_page: #{current_page}"
      response = RestClient.get "#{project_path}/issues/#{iid}/resource_label_events?#{filters}", {"PRIVATE-TOKEN" => ENV["PRIVATE_TOKEN"]}
      total_pages =  response.headers[:x_total_pages]

      JSON.parse(response).each{ |element|
        label_name =  element['label']['name']
        # puts "#{label_name} => #{element['action']}"
        if(element['action'] == "add")
          if label_events.key?(label_name)
            label_events[label_name] +=  1
          else
            label_events[label_name] = 1
          end
        end
      }
    end
    label_events_arr.push(label_events)
  }

  label_events_arr
end

project_path = "https://gitlab.com/api/v4/projects/gitlab-org%2Fquality%2Ftestcases"

issue_ids = get_issue_ids(project_path)
html = hasharray_to_html(get_label_events(project_path, issue_ids))

File.write('output.html', html)
